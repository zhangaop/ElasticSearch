- 插值表达式
    
    ````
    {{number + 1}}
    {{ok?'yes':'no'}} 
     ````  
- 常用指令
    ```` 
     v-on:click点击事件
     v-on:keydown键盘事件
     v-on:mouseover鼠标事件
     @mouseover移入
     @mouseleave移出
     event.preventDefault()取消事件的默认动作
     v-on:keyup.enter回车事件
     v-on的缩写@
     @submit="post($event)"//提交事件
     @submit.preven=""组织默认提交事件
     ````
     
     ```` 
     new Vue({
     el:"",//绑定标签属性
     data:"",//渲染的数据集合
     methods:""//事件函数方法
     })
     
     
     v-text:打印文本
     v-html:显示html便签
     ````
     ````
     v-bind: 绑定事件
     缩写 v-bind:color="yy"  /  :color="yy"
     v-bind={href:"url"+id}   /  :href="url"+id
     ````
     ````
     v-model
     v-model.lazy  #惰性事件，不自动更新（一般用于注册时输入完成时验证，可以提高一点点性能）
     v-model.trim  #去除左右两边的空格
     v-model.number #将字符串自动转换成整行
     ````
     
     ````
     v-for
     v-for="(item,intex) in list "  /  list数据对象  item 单个对象  index 索引数
     v-if=""  / 更具表达式判断是否来渲染元素
     v-show /  更具表达式的值来切换元素display css属性
     ````
     ````
     VUE生命周期
     ````