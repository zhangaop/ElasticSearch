package com.company.project.service.impl;

import com.company.project.dao.StuMapper;
import com.company.project.model.Stu;
import com.company.project.service.StuService;
import com.company.project.core.AbstractService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * Created by CodeGenerator on 2020/05/31.
 */
@Service
@Transactional
public class StuServiceImpl extends AbstractService<Stu> implements StuService {
    @Resource
    private StuMapper stuMapper;

}
